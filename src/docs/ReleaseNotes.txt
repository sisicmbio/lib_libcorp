*system:    LIBCorp
*version:   1.10.6
*iteration: N/A
*deliver:   28-06-2016 [16:40]

*objective:
- Correção no serviço de exclusão de endereço 'libCorpDeleteEndereco'


Versão 1.10.5
- Correção na persistência do co_cnuc na atualização dos dados de Unidade Interna

Versão 1.10.4
- Corrindo query de validação de existência unidade filha ativa no Webservice de cadastro de Unidade Org Interna para inativação de unidade pai

Versão 1.10.3
- Ajuste em mensagens de validação do Webservice de cadastro de Unidade Org Interna

Versão 1.10.2
- Ajuste na query de verificação de hierarquia na validação de inativação de Unidade Interna

Versão 1.10.1
- Ajuste nas mensagens de validação no serviço de Cadastro de Unidade Org Interna
- Implementação de regra no serviço para inativação de Unidade Org Interna

Versão 1.10.0
- Implementação da estrutura para o Webservice libCorpUnidadeOrgByTipoUorg

Versão 1.9.3
- Correção no metodo EnderecoBusiness::updateEndereco que não persistia o complemento e o número na atualização caso
  não fosse informado e anteriormente o endereço possuía tais informações

Versão 1.9.2
- Alteração no UnidadeOrgBusiness::saveUnidadeOrgInterna retirando o parâmetro de municípios.
- Alterando os sql para string nos métodos UnidadeOrgPersist::getUnidadeOrgByName e UnidadeOrgPersist::getUnidadeOrgBySigla

Versão 1.9.1
- Alteração no updatePessoaFisica, para alterar o tipo de pessoa de acordo com a nacionalidade.

Versão 1.9.0
- Inclusão de funcionalidades de manutenção de Unidade Organizacional Interna
- Inclusão de funcionalidades de inclusão/exclusão de Municipio vinculado a Unidade Organizacional Interna

Versão 1.8.4
- Alteração para tratamento do tipo de pessoa, quando não for enviado a nascionalidade.

Versão 1.8.3
- Alteração no serviço de Pessoa Física, tratamento para Pessoa Estrangeira.

Versão 1.8.2
- Alteração do VO de Pessoa referente ao stHomologado.

*products:
 - CODIGO-FONTE

*new_functionality:
 - Manter Unidade Organizacional Interna

*changed_funcionality:
- Manter Pessoa
