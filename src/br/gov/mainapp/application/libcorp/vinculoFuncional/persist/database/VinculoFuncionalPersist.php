<?php
/*
 * Copyright 2011 ICMBio
 * Este arquivo é parte do programa SISICMBio
 * O SISICMBio é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos
 * da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão
 * 2 da Licença.
 *
 * Este programa é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; sem
 * uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no endereço
 * www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF)
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 * */
namespace br\gov\mainapp\application\libcorp\vinculoFuncional\persist\database;
use br\gov\sial\core\valueObject\ValueObjectAbstract,
    br\gov\sial\core\persist\exception\PersistException,
    br\gov\sial\core\persist\database\Persist as ParentPersist,
    br\gov\mainapp\application\libcorp\vinculoFuncional\valueObject\VinculoFuncionalValueObject;

/**
  * SISICMBio
  *
  * Generated By SIAL Generator - vs 0.2.0
  *
  * @name CargoPersist
  * @package br.gov.icmbio.sisicmbio.application.sica.cargo
  * @subpackage persist
  * @author Fabio Lima <fabioolima@gmail.com>
  * @version $Id$
  * */
class VinculoFuncionalPersist extends ParentPersist
{

    /**
     * Busca para Graphico Mensal
     * @param ReportValueObject $voReport
     */
    public function save (ValueObjectAbstract $voVinculoFuncional)
    {
        $attrs = $voVinculoFuncional->annotation()->load();

        $query = "INSERT INTO %s.%s (%s) VALUES (%s) RETURNING %s";
        $arrCmp = $arrVlw = array();
        $prmKey = NULL;
        
        foreach ($attrs->attrs as $key => $value) {
            $getMethod = $value->get;
            if (isset($value->primaryKey)) {
                $prmKey = $value->database;
                continue;
            }

            if ($voVinculoFuncional->$getMethod() == "") {
                continue;
            }

            $arrCmp[] = $value->database;
            $arrFid[] = ":{$key}";
            $arrVlw[$key] = (object) array('type' => $value->type, 'value' => $voVinculoFuncional->$getMethod());
        }

        $query = sprintf($query, $attrs->schema, $attrs->entity, implode(',', $arrCmp), implode(',', $arrFid), $prmKey);

        try {
            return $this->execute($query, $arrVlw);
        } catch (\Exception $excp) {
            throw new PersistException ($excp->getMessage());
        }
    }

    /**
     * Busca para Graphico Mensal
     * @param ReportValueObject $voReport
     */
    public function update (ValueObjectAbstract $voVinculoFuncional)
    {
        $attrs  = $voVinculoFuncional->annotation()->load();
        $query  = "UPDATE %s.%s SET %s WHERE %s = :%s";
        $arrCmp = $arrVlw = array();
        $prmKey = NULL;
        $prmPK  = NULL;
        
        foreach ($attrs->attrs as $key => $value) {
            $getMethod = $value->get;
            if (isset($value->primaryKey)) {
                $prmKey  = $value->database;
                $prmPK = $key;
                $arrVlw[$key] = (object) array('type' => $value->type, 'value' => $voVinculoFuncional->$getMethod());
                continue;
            }

            if ($voVinculoFuncional->$getMethod() == "") {
                continue;
            }

            $arrCmp[] = " {$value->database} = :{$key}";
            $arrVlw[$key] = (object) array('type' => $value->type, 'value' => $voVinculoFuncional->$getMethod());
        }

        $query = sprintf($query, $attrs->schema, $attrs->entity, implode(',', $arrCmp), $prmKey, $prmPK);

        try {
            PersistException::throwsExceptionIfParamIsNull($prmPK, "A Primary Key não foi informada.");
            return $this->execute($query, $arrVlw);
        } catch (\Exception $excp) {
            throw new PersistException ($excp->getMessage());
        }
    }

    /**
     * Função que monta os dados para pesquisa
     * @param ValueObjectAbstract $valueObject
     */
    private function _preparePersist (ValueObjectAbstract $valueObject)
    {
        $arrResult = array();
        $arrVo     = array_filter($valueObject->toArray());
        $attrs = (array) $valueObject->annotation()->load()->attrs;

        foreach ($arrVo as $key => $elmnt) {
            $arrResult[$key] = (object) array('type' => $attrs[$key]->type, 'value' => $elmnt);
        }

        return $arrResult;
    }
}