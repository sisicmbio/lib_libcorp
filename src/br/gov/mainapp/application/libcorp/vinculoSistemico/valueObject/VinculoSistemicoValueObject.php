<?php

/*
 * Copyright 2011 ICMBio
 * Este arquivo é parte do programa SISICMBio
 * O SISICMBio é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos
 * da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão
 * 2 da Licença.
 *
 * Este programa é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; sem
 * uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no endereço
 * www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF)
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 * */
namespace br\gov\mainapp\application\libcorp\vinculoSistemico\valueObject;
use br\gov\sial\core\valueObject\ValueObjectAbstract as ParentValueObject,
    br\gov\mainapp\application\libcorp\pessoa\mvcb\business\PessoaBusiness,
    br\gov\mainapp\application\libcorp\tipoVinculoSistemico\mvcb\business\TipoVinculoSistemicoBusiness;

/**
 * SISICMBio
 *
 * Generated By SIAL Generator - vs 0.2.0
 *
 * @name VinculoSistemicoValueObject
 * @package br.gov.icmbio.sisicmbio.application.sica.vinculoSistemico
 * @subpackage valueObject
 * @schema(name="corporativo")
 * @entity(name="vinculo_sistemico")
 * @author Fabio Lima <fabioolima@gmail.com>
 * @author J. Augusto <augustowebd@gmail.com>
 * @version $Id$
 * */
class VinculoSistemicoValueObject extends ParentValueObject {

    /**
     * @attr (
     *  name="sqVinculoSistemico",
     *  database="sq_vinculo_sistemico",
     *  primaryKey="TRUE",
     *  type="integer",
     *  nullable="FALSE",
     *  get="getSqVinculoSistemico",
     *  set="setSqVinculoSistemico"
     * )
     * */
    private $_sqVinculoSistemico;

    /**
     * @attr (
     *  name="sqPessoa",
     *  database="sq_pessoa",
     *  type="integer",
     *  nullable="FALSE",
     *  get="getSqPessoa",
     *  set="setSqPessoa"
     * )
     * */
    private $_sqPessoa;

    /**
     * @attr (
     *  name="sqPessoaRelacionamento",
     *  database="sq_pessoa_relacionamento",
     *  type="integer",
     *  nullable="FALSE",
     *  get="getSqPessoaRelacionamento",
     *  set="setSqPessoaRelacionamento"
     * )
     * */
    private $_sqPessoaRelacionamento;

    /**
     * @attr (
     *  name="sqTipoVinculoSistemico",
     *  database="sq_tipo_vinculo_sistemico",
     *  type="integer",
     *  nullable="FALSE",
     *  get="getSqTipoVinculoSistemico",
     *  set="setSqTipoVinculoSistemico"
     * )
     * */
    private $_sqTipoVinculoSistemico;

    /**
     * @attr (
     *  name="dtInicioVinculo",
     *  database="dt_inicio_vinculo",
     *  type="date",
     *  nullable="FALSE",
     *  get="getDtInicioVinculo",
     *  set="setDtInicioVinculo"
     * )
     * */
    private $_dtInicioVinculo;

    /**
     * @attr (
     *  name="dtFimVinculo",
     *  database="dt_fim_vinculo",
     *  type="date",
     *  nullable="TRUE",
     *  get="getDtFimVinculo",
     *  set="setDtFimVinculo"
     * )
     * */
    private $_dtFimVinculo;

    /**
     * @attr (
     *  name="noCargo",
     *  database="no_cargo",
     *  type="string",
     *  nullable="TRUE",
     *  get="getNoCargo",
     *  set="setNoCargo"
     * )
     * */
    private $_noCargo;

    /**
     * @param integer $SqVinculoSistemico
     * @param integer $sqPessoa
     * @param integer $sqPessoaRelacionamento
     * @param integer $sqTipoVinculoSistemico
     * @param integer $dtInicioVinculo
     * @param integer $dtFimVinculo
     * @param string  $noCargo
     * */
    public function __construct($sqVinculoSistemico = NULL,
                                $sqPessoa = NULL,
                                $sqPessoaRelacionamento = NULL,
                                $sqTipoVinculoSistemico = NULL,
                                $dtInicioVinculo = NULL,
                                $dtFimVinculo = NULL,
                                $noCargo = NULL)
    {
        parent::__construct();
        $this->setSqVinculoSistemico($sqVinculoSistemico)
             ->setSqPessoa($sqPessoa)
             ->setSqPessoaRelacionamento($sqPessoaRelacionamento)
             ->setSqTipoVinculoSistemico($sqTipoVinculoSistemico)
             ->setDtInicioVinculo($dtInicioVinculo)
             ->setDtFimVinculo($dtFimVinculo)
             ->setNoCargo($noCargo);
    }

    /**
     * @return integer
     * */
    public function getSqVinculoSistemico()
    {
        return $this->_sqVinculoSistemico;
    }

    /**
     * @return integer
     * */
    public function getSqPessoa()
    {

        if ((NULL != $this->_sqPessoa) && !($this->_sqPessoa instanceof parent)) {
            $this->_sqPessoa = PessoaBusiness::factory(NULL,'libcorp')->find($this->_sqPessoa);
        }

        return $this->_sqPessoa;
    }

    /**
     * @return integer
     * */
    public function getSqPessoaRelacionamento()
    {

        if ((NULL != $this->_sqPessoaRelacionamento) && !($this->_sqPessoaRelacionamento instanceof parent)) {
            $this->_sqPessoaRelacionamento = PessoaBusiness::factory(NULL,'libcorp')->find($this->_sqPessoaRelacionamento);
        }

        return $this->_sqPessoaRelacionamento;
    }

    /**
     * @return integer
     * */
    public function getSqTipoVinculoSistemico()
    {

        if ((NULL != $this->_sqTipoVinculoSistemico) && !($this->_sqTipoVinculoSistemico instanceof parent)) {
            $this->_sqTipoVinculoSistemico = TipoVinculoSistemicoBusiness::factory(NULL,'libcorp')->find($this->_sqTipoVinculoSistemico);
        }

        return $this->_sqTipoVinculoSistemico;
    }

    /**
     * @return DateTime
     * */
    public function getDtInicioVinculo()
    {
        return $this->_dtInicioVinculo;
    }

    /**
     * @return integer
     * */
    public function getDtFimVinculo()
    {
        return $this->_dtFimVinculo;
    }

    /**
     * @return string
     * */
    public function getNoCargo()
    {
        return $this->_noCargo;
    }

    /**
     * @param integer $sqVinculoSistemico
     * @return br\gov\mainapp\application\sica\vinculoFuncional\valueObject\VinculoSistemicoValueObject
     * */
    public function setSqVinculoSistemico($sqVinculoSistemico = NULL)
    {
        $this->_sqVinculoSistemico = $sqVinculoSistemico;
        return $this;
    }

    /**
     * @param integer $sqPessoa
     * @return br\gov\mainapp\application\sica\vinculoFuncional\valueObject\VinculoSistemicoValueObject
     * */
    public function setSqPessoa($sqPessoa = NULL)
    {
        $this->_sqPessoa = $sqPessoa;
        return $this;
    }

    /**
     * @param integer $sqPessoaRelacionamento
     * @return br\gov\mainapp\application\sica\vinculoFuncional\valueObject\VinculoSistemicoValueObject
     * */
    public function setSqPessoaRelacionamento($sqPessoaRelacionamento = NULL)
    {
        $this->_sqPessoaRelacionamento = $sqPessoaRelacionamento;
        return $this;
    }

    /**
     * @param integer $sqTipoVinculoSistemico
     * @return br\gov\mainapp\application\sica\vinculoFuncional\valueObject\VinculoSistemicoValueObject
     * */
    public function setSqTipoVinculoSistemico($sqTipoVinculoSistemico = NULL)
    {
        $this->_sqTipoVinculoSistemico = $sqTipoVinculoSistemico;
        return $this;
    }

    /**
     * @param integer $dtInicioVinculo
     * @return br\gov\mainapp\application\sica\vinculoFuncional\valueObject\VinculoSistemicoValueObject
     * */
    public function setDtInicioVinculo($dtInicioVinculo = NULL)
    {
        $this->_dtInicioVinculo = $dtInicioVinculo;
        return $this;
    }

    /**
     * @param integer $dtFimVinculo
     * @return br\gov\mainapp\application\sica\vinculoFuncional\valueObject\VinculoSistemicoValueObject
     * */
    public function setDtFimVinculo($dtFimVinculo = NULL)
    {
        $this->_dtFimVinculo = $dtFimVinculo;
        return $this;
    }

    /**
     * @param string $noCargo
     * @return br\gov\mainapp\application\sica\vinculoFuncional\valueObject\VinculoSistemicoValueObject
     * */
    public function setNoCargo($noCargo = NULL)
    {
        $this->_noCargo = $noCargo;
        return $this;
    }
}