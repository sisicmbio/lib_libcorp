<?php
/*
 * Copyright 2015 ICMBio
 * Este arquivo é parte do programa SISICMBio
 * O SISICMBio é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos
 * da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão
 * 2 da Licença.
 *
 * Este programa é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; sem
 * uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no endereço
 * www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF)
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 * */
namespace br\gov\mainapp\application\libcorp\unidadeOrgExterna\valueObject;

use br\gov\sial\core\valueObject\ValueObjectAbstract,
    br\gov\mainapp\application\libcorp\pessoa\mvcb\business\PessoaBusiness,
    br\gov\mainapp\application\libcorp\tipoUnidadeOrg\mvcb\business\TipoUnidadeOrgBusiness;

/**
  * SISICMBio
  *
  * @name UnidadeOrgExternaValueObject
  * @package br.gov.mainapp.application.libcorp.unidadeOrgExterna
  * @subpackage valueObject
  * @schema(name="corporativo")
  * @entity(name="unidade_org_externa")
  * @author Diego Costa <diego.costa.terceirizado@icmbio.gov.br>
  * @since 2015-10-15
  * @log(name="all")
  * */
class UnidadeOrgExternaValueObject extends ValueObjectAbstract
{
	/**
	 * @attr (
	 *  name="sqPessoa",
	 *  database="sq_pessoa",
	 *  primaryKey="TRUE",
	 *  type="integer",
	 *  nullable="FALSE",
	 *  get="getSqPessoa",
	 *  set="setSqPessoa"
	 * )
	 * */
	private $_sqPessoa;

    /**
     * @attr (
     *  name="sqTipoUnidade",
     *  database="sq_tipo_unidade",
     *  type="integer",
     *  nullable="TRUE",
     *  get="getSqTipoUnidade",
     *  set="setSqTipoUnidade"
     * )
     * */
     private $_sqTipoUnidade;

    /**
     * @attr (
     *  name="sqEsfera",
     *  database="sq_esfera",
     *  type="integer",
     *  nullable="TRUE",
     *  get="getSqEsfera",
     *  set="setSqEsfera"
     * )
     * */
     private $_sqEsfera;

    /**
     * @attr (
     *  name="sgUnidadeOrg",
     *  database="sg_unidade_org",
     *  type="string",
     *  nullable="TRUE",
     *  get="getSgUnidadeOrg",
     *  set="setSgUnidadeOrg"
     * )
     * */
     private $_sgUnidadeOrg;

    /**
     * @attr (
     *  name="coCnuc",
     *  database="co_cnuc",
     *  type="integer",
     *  nullable="TRUE",
     *  get="getCoCnuc",
     *  set="setCoCnuc"
     * )
     * */
     private $_coCnuc;


    /**
     * @attr (
     *  name="stAtivo",
     *  database="st_ativo",
     *  type="boolean",
     *  nullable="TRUE",
     *  get="getStAtivo",
     *  set="setStAtivo"
     * )
     * */
    private $_stAtivo;

    /**
     * @param integer $sqPessoa
     * @param integer $sqTipoUnidade
     * @param integer $sqEsfera
     * @param string  $sgUnidadeOrg
     * @param integer $coCnuc
     * @param boolean $stAtivo
     * */
    public function __construct ($sqPessoa = NULL,
                                 $sqTipoUnidade = NULL,
                                 $sqEsfera = NULL,
                                 $sgUnidadeOrg = NULL,
                                 $coCnuc = NULL,
                                 $stAtivo = NULL)
    {
        parent::__construct();
        $this->setSqPessoa($sqPessoa)
             ->setSqTipoUnidade($sqTipoUnidade)
             ->setSqEsfera($sqEsfera)
             ->setSgUnidadeOrg($sgUnidadeOrg)
             ->setCoCnuc($coCnuc)
             ->setStAtivo($stAtivo)
             ;
    }

    /**
     * @return PessoaValueObject
     * */
    public function getSqPessoa ()
    {
        if ((NULL != $this->_sqPessoa) && !($this->_sqPessoa instanceof parent)) {
            $this->_sqPessoa = PessoaBusiness::factory(NULL, 'libcorp')->find($this->_sqPessoa);
        }
        return $this->_sqPessoa;
    }

    /**
     * @return TipoUnidadeValueObject
     * */
    public function getSqTipoUnidade ()
    {
        if ((NULL != $this->_sqTipoUnidade) && !($this->_sqTipoUnidade instanceof parent)) {
            $this->_sqTipoUnidade = TipoUnidadeOrgBusiness::factory(NULL, 'libcorp')->find($this->_sqTipoUnidade);
        }
        return $this->_sqTipoUnidade;
    }

    /**
     * @return integer
     * */
    public function getSqEsfera ()
    {
        return $this->_sqEsfera;
    }

    /**
     * @return string
     * */
    public function getSgUnidadeOrg ()
    {
        return $this->_sgUnidadeOrg;
    }

    /**
     * @return integer
     * */
    public function getCoCnuc ()
    {
        return $this->_coCnuc;
    }


    /**
     * @return boolean
     * */
    public function getStAtivo ()
    {
        return $this->_stAtivo;
    }

    /**
     * @param integer $sqPessoa
     * @return UnidadeOrgExternaValueObject
     * */
    public function setSqPessoa ($sqPessoa = NULL)
    {
        $this->_sqPessoa = $sqPessoa;
        return $this;
    }

    /**
     * @param integer $sqTipoUnidade
     * @return UnidadeOrgExternaValueObject
     * */
    public function setSqTipoUnidade ($sqTipoUnidade = NULL)
    {
        $this->_sqTipoUnidade = $sqTipoUnidade;
        return $this;
    }

    /**
     * @param integer $sqEsfera
     * @return UnidadeOrgExternaValueObject
     * */
    public function setSqEsfera ($sqEsfera = NULL)
    {
        $this->_sqEsfera = $sqEsfera;
        return $this;
    }

    /**
     * @param string $sgUnidadeOrg
     * @return UnidadeOrgExternaValueObject
     * */
    public function setSgUnidadeOrg ($sgUnidadeOrg = NULL)
    {
        $this->_sgUnidadeOrg = $sgUnidadeOrg;
        return $this;
    }

    /**
     * @param integer $coCnuc
     * @return UnidadeOrgExternaValueObject
     * */
    public function setCoCnuc ($coCnuc = NULL)
    {
        $this->_coCnuc = $coCnuc;
        return $this;
    }

    /**
     * @param boolean $stAtivo
     * @return UnidadeOrgExternaValueObject
     * */
    public function setStAtivo ($stAtivo = NULL)
    {
        $this->_stAtivo = $stAtivo;
        return $this;
    }
}