<?php
/*
 * Copyright 2015 ICMBio
 * Este arquivo é parte do programa SISICMBio
 * O SISICMBio é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos
 * da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão
 * 2 da Licença.
 *
 * Este programa é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; sem
 * uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no endereço
 * www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF)
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 * */
namespace br\gov\mainapp\application\libcorp\unidadeOrgExterna\mvcb\business;

use br\gov\sial\core\util\validate\Validate,
    br\gov\sial\core\mvcb\model\exception\ModelException,
    br\gov\sial\core\mvcb\business\exception\BusinessException,
    br\gov\mainapp\application\libcorp\pessoa\mvcb\business\PessoaBusiness,
    br\gov\mainapp\application\libcorp\pessoa\valueObject\PessoaValueObject,
    br\gov\mainapp\application\libcorp\pessoaJuridica\mvcb\business\PessoaJuridicaBusiness,
    br\gov\mainapp\application\libcorp\pessoaJuridica\valueObject\PessoaJuridicaValueObject,
    br\gov\mainapp\application\libcorp\unidadeOrgExterna\valueObject\UnidadeOrgExternaValueObject,
    br\gov\mainapp\application\libcorp\parent\mvcb\business\BusinessAbstract as ParentBusiness;

/**
  * SISICMBio
  *
  * @name UnidadeOrgExternaBusiness
  * @package br.gov.mainapp.application.libcorp.unidadeOrgExterna.mvcb
  * @subpackage business
  * @author Diego Costa <diego.costa.terceirizado@icmbio.gov.br>
  * @since 2015-10-15
  * */
class UnidadeOrgExternaBusiness extends ParentBusiness
{
    /**
     * Define o tipo de pessoa como Unidade Externa
     */
    const TIPO_PESSOA_UNIDADE_ORG_EXTERNA = 5;

    /**
     * @var string
     */
    const INVALID_CNPJ = 'O CNPJ informado é inválido.';

    /**
     * @var string
     */
    const MISSING_KEY = 'Na atualização é obrigatório informar o ID da Unidade Externa.';

    /**
     * Insere os dados de Unidade Org Externa
     * - Dados Obrigatórios : Nome
     * - Dados Validados  : CNPJ
     *
     * @param PessoaValueObject $voPessoa
     * @param UnidadeOrgExternaValueObject $voUnidadeOrgExterna
     * @throw BusinessException
     */
    public function saveUnidadeOrgExterna (PessoaValueObject            $voPessoa,
                                           UnidadeOrgExternaValueObject $voUnidadeOrgExterna,
                                           PessoaJuridicaValueObject $voPJ)
    {
        try
        {
            # Salva Pessoa
            $voPessoa->setSqTipoPessoa(self::TIPO_PESSOA_UNIDADE_ORG_EXTERNA);

            PessoaBusiness::factory(NULL, 'libcorp')->save($voPessoa);

            $sqPessoa = $voPessoa->getSqPessoa();

            if ($voPJ->getNuCnpj())
            {
                BusinessException::throwsExceptionIfParamIsNull(
                    Validate::isCnpj($voPJ->getNuCnpj()),self::INVALID_CNPJ
                );

                $voPJ->setSqPessoa($sqPessoa);

                PessoaJuridicaBusiness::factory(NULL, 'libcorp')->saveUpdate($voPJ);
            }

            # Salva UnidadeOrgExterna
            $voUnidadeOrgExterna->setSqPessoa($sqPessoa);

            $this->getModelPersist('libcorp')->save($voUnidadeOrgExterna);

            return $voUnidadeOrgExterna;

        } catch (ModelException $mExcp) {
            throw new BusinessException($mExcp->getMessage(), $mExcp->getCode());
        }
    }

    /**
     * Atualiza os dados de Unidade Org Externa
     *
     * @param PessoaValueObject $voPessoa
     * @param UnidadeOrgExternaValueObject $voUnidadeOrgExterna
     * @throws BusinessException
     */
    public function updateUnidadeOrgExterna (PessoaValueObject            $voPessoa,
                                             UnidadeOrgExternaValueObject $voUnidadeOrgExterna,
                                             PessoaJuridicaValueObject $voPJ)
    {
        try {
            $sqPessoa = $voPessoa->getSqPessoa();

            BusinessException::throwsExceptionIfParamIsNull($sqPessoa, self::MISSING_KEY);

            # atualiza a pessoa
            $voPessoa->setSqTipoPessoa(self::TIPO_PESSOA_UNIDADE_ORG_EXTERNA);
            PessoaBusiness::factory(NULL, 'libcorp')->update($voPessoa);

            if (trim($voPJ->getNuCnpj()))
            {
                BusinessException::throwsExceptionIfParamIsNull(
                    Validate::isCnpj($voPJ->getNuCnpj()),self::INVALID_CNPJ
                );

                PessoaJuridicaBusiness::factory(NULL, 'libcorp')->saveUpdate($voPJ);
            }

            $voUnidadeOrgExterna->setSqPessoa($sqPessoa);

            # Atualiza UnidadeOrgExterna
            $this->getModelPersist('libcorp')->update($voUnidadeOrgExterna);

            return $voUnidadeOrgExterna;

        } catch (ModelException $mExcp) {
            throw new BusinessException($mExcp->getMessage(), $mExcp->getCode());
        }
    }
}