<?php
/*
 * Copyright 2011 ICMBio
 * Este arquivo é parte do programa SIAL
 * O SIAL é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos
 * da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão
 * 2 da Licença.
 *
 * Este programa é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; sem
 * uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no endereço
 * www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF)
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 * */
namespace br\gov\mainapp\application\libcorp\configuracao\persist\database;

use br\gov\mainapp\library\persist\database\Persist as ParentPersist,
    br\gov\sial\core\valueObject\ValueObjectAbstract,
    br\gov\sial\core\persist\exception\PersistException;

/**
 * @package br.gov.mainapp.application.libcorp.configuracao.persist
 * @subpackage database
 * @name ConfiguracaoPersist
 * @author Juliano Buzanello <juliano.buzanello.terceirizado@icmbio.gov.br>
 */
class ConfiguracaoPersist extends ParentPersist
{
}
