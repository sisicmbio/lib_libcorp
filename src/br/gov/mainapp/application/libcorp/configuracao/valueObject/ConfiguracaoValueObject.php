<?php
/*
 * Copyright 2011 ICMBio
 * Este arquivo é parte do programa SISICMBio
 * O SISICMBio é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos
 * da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão
 * 2 da Licença.
 *
 * Este programa é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; sem
 * uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no endereço
 * www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF)
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 * */
namespace br\gov\mainapp\application\libcorp\configuracao\valueObject;
use br\gov\sial\core\valueObject\ValueObjectAbstract;

/**
 * @name ConfiguracaoValueObject
 * @author Juliano Buzanello <juliano.buzanello.terceirizado@icmbio.gov.br>
 * @schema(name="sicae")
 * @entity(name="vw_configuracao")
 */
class ConfiguracaoValueObject extends ValueObjectAbstract
{

    /**
     * @attr (
     *  name="sqConstante",
     *  database="sq_configuracao",
     *  type="integer",
     *  primaryKey="TRUE",
     *  get="getSqConstante",
     *  set="setSqConstante"
     * )
     * */
    private $_sqConstante;

    /**
     * @attr (
     *  name="noConstante",
     *  database="no_constante",
     *  type="string",
     *  get="getNoConstante",
     *  set="setNoConstante"
     * )
     * */
    private $_noConstante;

    /**
     * @attr (
     *  name="noOwner",
     *  database="no_owner",
     *  type="string",
     *  get="getNoOwner",
     *  set="setNoOwner"
     * )
     * */
    private $_noOwner;

    /**
     * @attr (
     *  name="noTabela",
     *  database="no_tabela",
     *  type="string",
     *  get="getNoTabela",
     *  set="setNoTabela"
     * )
     * */
    private $_noTabela;

    /**
     * @attr (
     *  name="noColuna",
     *  database="no_coluna",
     *  type="string",
     *  get="getNoColuna",
     *  set="setNoColuna"
     * )
     * */
    private $_noColuna;


    /**
     * @attr (
     *  name="sqValor",
     *  database="sq_valor",
     *  type="integer",
     *  get="getSqValor",
     *  set="setSqValor"
     * )
     * */
    private $_sqValor;

    function getSqConstante() {
        return $this->_sqConstante;
    }

    function getNoConstante() {
        return $this->_noConstante;
    }

    function getNoOwner() {
        return $this->_noOwner;
    }

    function getNoTabela() {
        return $this->_noTabela;
    }

    function getNoColuna() {
        return $this->_noColuna;
    }

    function getSqValor() {
        return $this->_sqValor;
    }

    function setSqConstante($sqConstante) {
        $this->_sqConstante = $sqConstante;
    }

    function setNoConstante($noConstante) {
        $this->_noConstante = $noConstante;
    }

    function setNoOwner($noOwner) {
        $this->_noOwner = $noOwner;
    }

    function setNoTabela($noTabela) {
        $this->_noTabela = $noTabela;
    }

    function setNoColuna($noColuna) {
        $this->_noColuna = $noColuna;
    }

    function setSqValor($sqValor) {
        $this->_sqValor = $sqValor;
    }
}