<?php

/*
 * Copyright 2011 ICMBio
 * Este arquivo é parte do programa SISICMBio
 * O SISICMBio é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos
 * da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão
 * 2 da Licença.
 *
 * Este programa é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; sem
 * uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no endereço
 * www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF)
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 * */

namespace br\gov\mainapp\application\libcorp\unidadeOrgMunicipio\valueObject;

use br\gov\sial\core\valueObject\ValueObjectAbstract as ParentValueObject;

/**
 * SISICMBio
 *
 * @name UnidadeOrganizacionalValueObject
 * @schema(name="corporativo")
 * @entity(name="unid_org_municipio")
 * @author Juliano Buzanello <jbcisne@gmail.com.br>
 * @since 2016-03-01
 */
class UnidadeOrgMunicipioValueObject extends ParentValueObject
{

    /**
     * @attr (
     *  name="sqUnidOrgMunicipio",
     *  database="sq_unid_org_municipio",
     *  primaryKey=true,
     *  autoIncrement=true,
     *  type="integer",
     *  get="getSqUnidOrgMunicipio",
     *  set="setSqUnidOrgMunicipio"
     * )
     * */
    private $_sqUnidOrgMunicipio;

    /**
     * @attr (
     *  name="sqMunicipio",
     *  database="sq_municipio",
     *  type="integer",
     *  get="getSqMunicipio",
     *  set="setSqMunicipio"
     * )
     * */
    private $_sqMunicipio;

    /**
     * @attr (
     *  name="sqUnidadeOrg",
     *  database="sq_unidade_org",
     *  type="integer",
     *  get="getSqUnidadeOrg",
     *  set="setSqUnidadeOrg"
     * )
     * */
    private $_sqUnidadeOrg;

    /**
     * @attr (
     *  name="sqIntervencao",
     *  database="sq_intervencao",
     *  type="integer",
     *  get="getSqIntervencao",
     *  set="setSqIntervencao"
     * )
     * */
    private $_sqIntervencao;

    /**
     * @attr (
     *  name="inPrincipal",
     *  database="in_principal",
     *  type="boolean",
     *  get="getInPrincipal",
     *  set="setInPrincipal"
     * )
     * */
    private $_inPrincipal;

    public function __construct ($sqUnidOrgMunicipio = NULL
                               , $sqMunicipio = NULL
                               , $sqUnidadeOrg = NULL
                               , $sqIntervencao = NULL
                               , $inPrincipal = NULL)
    {
        parent::__construct();

        $this->setSqUnidOrgMunicipio($sqUnidOrgMunicipio);
        $this->setSqMunicipio($sqMunicipio);
        $this->setSqUnidadeOrg($sqUnidadeOrg);
        $this->setSqIntervencao($sqIntervencao);
        $this->setInPrincipal($inPrincipal);
    }

    /**
     * @return integer $_sqUnidOrgMunicipio
     */
    public function getSqUnidOrgMunicipio ()
    {
        return $this->_sqUnidOrgMunicipio;
    }

    /**
     * @return integer $_sqMunicipio
     */
    public function getSqMunicipio ()
    {
        return $this->_sqMunicipio;
    }

    /**
     * @return integer $_sqUnidadeOrg
     */
    public function getSqUnidadeOrg ()
    {
        return $this->_sqUnidadeOrg;
    }

    /**
     * @return integer $_sqIntervencao
     */
    public function getSqIntervencao ()
    {
        return $this->_sqIntervencao;
    }

    /**
     * @param boolean $_inPrincipal
     * @return UnidadeOrgMunicipioValueObject
     */
    public function getInPrincipal ()
    {
        return $this->_inPrincipal;
    }

    /**
     * @param integer $sqUnidOrgMunicipio
     * @return UnidadeOrgMunicipioValueObject
     * */
    public function setSqUnidOrgMunicipio ($sqUnidOrgMunicipio = NULL)
    {
        $this->_sqUnidOrgMunicipio = $sqUnidOrgMunicipio;
    }

    /**
     * @param integer $sqMunicipio
     * @return UnidadeOrgMunicipioValueObject
     * */
    public function setSqMunicipio ($sqMunicipio = NULL)
    {
        $this->_sqMunicipio = $sqMunicipio;
    }

    /**
     * @param integer $sqUnidadeOrg
     * @return UnidadeOrgMunicipioValueObject
     * */
    public function setSqUnidadeOrg ($sqUnidadeOrg = NULL)
    {
        $this->_sqUnidadeOrg = $sqUnidadeOrg;
    }

    /**
     * @param integer $sqIntervencao
     * @return UnidadeOrgMunicipioValueObject
     * */
    public function setSqIntervencao ($sqIntervencao = NULL)
    {
        $this->_sqIntervencao = $sqIntervencao;
    }

    /**
     *
     * @param boolean $inPrincipal
     */
    public function setInPrincipal ($inPrincipal)
    {
        $this->_inPrincipal = $inPrincipal;
    }

}
