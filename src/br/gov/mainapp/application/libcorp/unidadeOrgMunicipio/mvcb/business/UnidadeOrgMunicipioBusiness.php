<?php
/*
 * Copyright 2011 ICMBio
 * Este arquivo é parte do programa SISICMBio
 * O SISICMBio é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos
 * da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão
 * 2 da Licença.
 *
 * Este programa é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; sem
 * uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no endereço
 * www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF)
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 * */
namespace br\gov\mainapp\application\libcorp\unidadeOrgMunicipio\mvcb\business;
use br\gov\icmbio\sial\valueObject\ValueObjectAbstract,
    br\gov\sial\core\mvcb\model\exception\ModelException,
    br\gov\sial\core\mvcb\business\exception\BusinessException,
    br\gov\mainapp\application\libcorp\parent\mvcb\business\BusinessAbstract as ParentBusiness,
    br\gov\mainapp\application\libcorp\unidadeOrgMunicipio\valueObject\UnidadeOrgMunicipioValueObject
    ;

/**
 *
 * @package br.gov.mainapp.application.libcorp.unidadeOrgMunicipio.mvcb
 * @subpackage business
 * @name UnidadeOrgMunicipioBusiness
 * @author Juliano Buzanello <jbcisne@gmail.com>
 * @since 2016-03-01
 */
class UnidadeOrgMunicipioBusiness extends ParentBusiness
{

    /**
     * @var string
     */
    const KEY_REQUIRED       = 'Registro não localizado para exclusão.';
    const MUNICIPIO_REQUIRED = 'É obrigatório informar um Município.';
    const MUNICIPIO_UNIQUE_PRINCIPAL   = 'A unidade já possui um município definido como principal.';
    const UNIDADE_REQUIRED   = 'A Unidade é obrigatória na definção de seu Município.';

    /**
     * Insere ou atualiza Pessoa
     * @param PessoaValueObject
     */
    public function save (UnidadeOrgMunicipioValueObject $vo)
    {
        try {
            $sqUnidOrgMunicipio = $vo->getSqUnidOrgMunicipio();
            $this->_validateSaveEntity($vo);

            if (empty($sqUnidOrgMunicipio)) {
                $this->getModelPersist('libcorp')->save($vo);
            } else {
                $this->getModelPersist('libcorp')->update($vo);
            }
            return $vo;
        } catch (ModelException $mExcp) {
            throw new BusinessException($mExcp->getMessage(), $mExcp->getCode());
        }
    }

    public function delete (UnidadeOrgMunicipioValueObject $vo)
    {
        try {
            BusinessException::throwsExceptionIfParamIsNull(trim($vo->getSqUnidOrgMunicipio()), self::KEY_REQUIRED);

            $this->getModelPersist('libcorp')->delete($vo);
        } catch (ModelException $mExcp) {
            throw new BusinessException($mExcp->getMessage(), $mExcp->getCode());
        }
    }

    private function _validateSaveEntity (UnidadeOrgMunicipioValueObject $vo)
    {
        try {
            BusinessException::throwsExceptionIfParamIsNull(trim($vo->getSqMunicipio()), self::MUNICIPIO_REQUIRED);
            BusinessException::throwsExceptionIfParamIsNull(trim($vo->getSqUnidadeOrg()), self::UNIDADE_REQUIRED);

            //verificar se já existe municipio principal
            if ($vo->getInPrincipal()) {

                $result = $this->getModelPersist('libcorp')
                               ->findPrincipalByUnidade($vo)
                               ->getAllDataViewObject();

                if ($result->count()) {
                    throw new BusinessException(self::MUNICIPIO_UNIQUE_PRINCIPAL);
                }
            }
        } catch (ModelException $mExcp) {
            throw new BusinessException($mExcp->getMessage(), $mExcp->getCode());
        } catch (BusinessException $bExcp) {
            throw $bExcp;
        }
    }
}