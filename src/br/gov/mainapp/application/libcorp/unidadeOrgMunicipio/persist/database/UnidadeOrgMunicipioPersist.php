<?php
/*
 * Copyright 2011 ICMBio
 * Este arquivo é parte do programa SISICMBio
 * O SISICMBio é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos
 * da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão
 * 2 da Licença.
 *
 * Este programa é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; sem
 * uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no endereço
 * www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF)
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 * */
namespace br\gov\mainapp\application\libcorp\unidadeOrgMunicipio\persist\database;

use br\gov\sial\core\persist\exception\PersistException;
use br\gov\mainapp\library\persist\database\Persist as ParentPersist;

use br\gov\mainapp\application\libcorp\pessoa\valueObject\PessoaValueObject;
use br\gov\mainapp\application\libcorp\unidadeOrgMunicipio\valueObject\UnidadeOrgMunicipioValueObject;


/**
  * SISICMBio
  *
  * @package br.gov.mainapp.application.libcorp.unidadeOrgMunicipio.persist
  * @subpackage database
  * @name UnidadeOrgMunicipioPersist
  * @author Juliano Buzanello <jbcisne@gmail.com>
  * @since 2016-03-01
  */
class UnidadeOrgMunicipioPersist extends ParentPersist
{


    /**
     * @param UnidadeOrgMunicipioValueObject $vo
     * @return UnidadeOrgModel
     * */
    public function findPrincipalByUnidade (UnidadeOrgMunicipioValueObject $vo)
    {
        try {
            # cria entidade de banco do VO informado
             $eUnidadeOrgMunicipio    = $this->getEntity($vo);


             $sqUnidadeOrg = $vo->getSqUnidadeOrg();

             if($sqUnidadeOrg instanceof PessoaValueObject) {
                 $sqUnidadeOrg = $sqUnidadeOrg->getSqPessoa();
             }

             # cria consulta
             $query = $this->getQuery($eUnidadeOrgMunicipio)
                           ->where($eUnidadeOrgMunicipio->column('sqUnidadeOrg')
                                                        ->equals($sqUnidadeOrg))
                           ->and($eUnidadeOrgMunicipio->column('inPrincipal')
                                                        ->equals(true));

            return $this->execute($query);
        } catch (\Exception $exc) {
            throw new PersistException($exc->getMessage(), $exc->getCode(), $exc);
        }
    }

}