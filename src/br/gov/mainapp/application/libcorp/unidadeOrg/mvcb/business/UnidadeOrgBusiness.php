<?php
/*
 * Copyright 2011 ICMBio
 * Este arquivo é parte do programa SISICMBio
 * O SISICMBio é um software livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos
 * da Licença Pública Geral GNU como publicada pela Fundação do Software Livre (FSF); na versão
 * 2 da Licença.
 *
 * Este programa é distribuído na esperança que possa ser útil, mas SEM NENHUMA GARANTIA; sem
 * uma garantia implícita de ADEQUAÇÃO a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU/GPL em português para maiores detalhes.
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU, sob o título "LICENCA.txt",
 * junto com este programa, se não, acesse o Portal do Software Público Brasileiro no endereço
 * www.softwarepublico.gov.br ou escreva para a Fundação do Software Livre(FSF)
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 * */
namespace br\gov\mainapp\application\libcorp\unidadeOrg\mvcb\business;
use br\gov\sial\core\mvcb\model\exception\ModelException,
    br\gov\sial\core\mvcb\business\exception\BusinessException,
    br\gov\sial\core\exception\IllegalArgumentException,

    #business
    br\gov\mainapp\application\libcorp\pessoa\mvcb\business\PessoaBusiness,
    br\gov\mainapp\application\libcorp\configuracao\mvcb\business\ConfiguracaoBusiness,
    br\gov\mainapp\application\libcorp\parent\mvcb\business\BusinessAbstract as ParentBusiness,

    #valueObject
    br\gov\mainapp\application\libcorp\estado\valueObject\EstadoValueObject,
    br\gov\mainapp\application\libcorp\pessoa\valueObject\PessoaValueObject,
    br\gov\mainapp\application\libcorp\municipio\valueObject\MunicipioValueObject,
    br\gov\mainapp\application\libcorp\unidadeOrg\valueObject\UnidadeOrgValueObject,
    br\gov\mainapp\application\libcorp\tipoUnidadeOrg\valueObject\TipoUnidadeOrgValueObject;


/**
  * SISICMBio
  *
  * @name UnidadeOrgBusiness
  * @package br.gov.icmbio.sisicmbio.application.libcorp.unidadeOrg.mvcb
  * @subpackage business
  * @author J. Augusto <augustowebd@gmail.com>
  * @version $Id$
  * */
class UnidadeOrgBusiness extends ParentBusiness
{

    /**
     * @var string
     */
    const INVALID_CNPJ              = 'O CNPJ informado é inválido.';

    const CODE_REQUIRED_SQ_PESSOA                 = '1100';
    const CODE_VALIDATE_NO_PESSOA                 = '1101';
    const CODE_VALIDATE_SG_UNIDADE_ORG            = '1102';
    const CODE_VALIDATE_SQ_TIPO_UNIDADE           = '1103';
    const CODE_VALIDATE_NO_PESSOA_DUPLICATED      = '1104';
    const CODE_VALIDATE_SG_UNIDADE_ORG_DUPLICATED = '1105';
    const CODE_VALIDATE_UNIDADE_INATIVA           = '1106';
    const CODE_VALIDATE_INATIVA_SUPERIOR          = '1107';
    const CODE_VALIDATE_INATIVA_ADM_PAI           = '1108';
    const CODE_VALIDATE_INATIVA_FIN_PAI           = '1109';

    private static $_arrMsg = array(
        self::CODE_REQUIRED_SQ_PESSOA                 => 'Na atualização é obrigatório informar o ID da Unidade a ser alterada.',
        self::CODE_VALIDATE_NO_PESSOA                 => 'O Nome da Unidade é de preenchimento obrigatório.',
        self::CODE_VALIDATE_SG_UNIDADE_ORG            => 'A Sigla da Unidade é de preenchimento obrigatório.',
        self::CODE_VALIDATE_SQ_TIPO_UNIDADE           => 'O Tipo de Unidade é de preenchimento obrigatório.',
        self::CODE_VALIDATE_NO_PESSOA_DUPLICATED      => 'Já existe unidade organizacional interna com o nome informado. Esta pode estar inativa.',
        self::CODE_VALIDATE_SG_UNIDADE_ORG_DUPLICATED => 'Já existe unidade organizacional interna com a sigla informada. Esta pode estar inativa.',
        self::CODE_VALIDATE_UNIDADE_INATIVA           => 'Unidade já encontra-se inativa.',
        self::CODE_VALIDATE_INATIVA_SUPERIOR          => 'Esta unidade é uma "Unidade Superior" de outra(s) unidade(s) que ainda está(ão) ativa(s).',
        self::CODE_VALIDATE_INATIVA_ADM_PAI           => 'Esta unidade é uma "Unidade Administrativa Pai" de outra(s) unidade(s) que ainda está(ão) ativa(s).',
        self::CODE_VALIDATE_INATIVA_FIN_PAI           => 'Esta unidade é uma "Unidade Financeira Pai" de outra(s) unidade(s) que ainda está(ão) ativa(s).',
    );

    /**
     * Insere os dados de Unidade Org
     * - Dados Obrigatórios : Nome, sigla, tipo de unidade
     * - Dados Validados  : CNPJ
     *
     * @param PessoaValueObject $voPessoa
     * @param UnidadeOrgExternaValueObject $voUnidadeOrg
     * @throw BusinessException
     */
    public function saveUnidadeOrgInterna (PessoaValueObject $voPessoa, UnidadeOrgValueObject $voUnidadeOrg ) {
        $this->transaction();
        try
        {
            $this->_filterDadosUnidade($voUnidadeOrg);

            $this->_validateToSave($voUnidadeOrg, $voPessoa);

            # Salva Pessoa
            $voPessoa->setSqTipoPessoa(ConfiguracaoBusiness::factory()->get('CORPORATIVO_TIPO_PESSOA_UNIDADE_ORGANIZACIONAL'))
                     ->setStRegistroAtivo(TRUE);

            PessoaBusiness::factory(NULL, 'libcorp')->save($voPessoa);

            $sqPessoa = $voPessoa->getSqPessoa();
            $voUnidadeOrg->setSqPessoa($sqPessoa);

            # Salva UnidadeOrgInterna
            $this->getModelPersist('libcorp')->mSave($voUnidadeOrg);

            $this->commit();
            return $voUnidadeOrg;

        } catch (BusinessException $bExcp) {
            $this->rollback();
            throw $bExcp;
        } catch (ModelException $mExcp) {
            $this->rollback();
            throw new BusinessException($mExcp->getMessage(), $mExcp->getCode());
        }
    }

    /**
     * Insere os dados de Unidade Org
     * - Dados Obrigatórios : Nome, sigla, tipo de unidade
     * - Dados Validados  : CNPJ
     *
     * @param PessoaValueObject $pessoaVo
     * @param UnidadeOrgExternaValueObject $unidadeOrgVo
     * @throw BusinessException
     */
    public function updateUnidadeOrgInterna (PessoaValueObject       $pessoaVo,
                                             UnidadeOrgValueObject     $unidadeOrgVo)
    {
        $this->transaction();
        try {

            $voUnidadeOrgClone = clone $unidadeOrgVo;

            # anula UnidadeFinPai
            if (0 === $voUnidadeOrgClone->getSqUnidadeFinPai()) {
                $unidadeOrgVo->setSqUnidadeFinPai(NULL);
            }
            # anula UnidadeAdmPai
            if (0 === $voUnidadeOrgClone->getSqUnidadeAdmPai()) {
                $unidadeOrgVo->setSqUnidadeAdmPai(NULL);
            }
            # anula UnidadeSuperior
            if (0 === $voUnidadeOrgClone->getSqUnidadeSuperior()) {
                $unidadeOrgVo->setSqUnidadeSuperior(NULL);
            }
            # anula CoUnidadeGestora
            if (0 === $voUnidadeOrgClone->getCoUnidadeGestora()) {
                $unidadeOrgVo->setCoUnidadeGestora(NULL);
            }
            # anula CoCnuc
            if (0 === $voUnidadeOrgClone->getCoCnuc()) {
                $unidadeOrgVo->setCoCnuc(NULL);
            }

            $this->_validateToUpdate($unidadeOrgVo, $pessoaVo);

            $voPessoaTmp = PessoaBusiness::factory(NULL, 'libcorp')->find($pessoaVo->getSqPessoa());
            $pessoaVo->copySaveObjectData($voPessoaTmp);

            PessoaBusiness::factory(NULL, 'libcorp')->update($pessoaVo);

            # Salva UnidadeOrgInterna
            $this->getModelPersist('libcorp')->mUpdate($unidadeOrgVo);

            $this->commit();

            return $this->find($unidadeOrgVo->getSqPessoa()->getSqPessoa());

        } catch (BusinessException $bExcp) {
            $this->rollback();
            throw $bExcp;
        } catch (ModelException $mExcp) {
            $this->rollback();
            throw new BusinessException($mExcp->getMessage(), $mExcp->getCode());
        }
    }

    private function _filterDadosUnidade(UnidadeOrgValueObject $vo)
    {
        if (!$vo->getSqUnidadeAdmPai() instanceof UnidadeOrgValueObject) {
            $vo->setSqUnidadeAdmPai(NULL);
        }

        if (!$vo->getSqUnidadeFinPai() instanceof UnidadeOrgValueObject) {
            $vo->setSqUnidadeFinPai(NULL);
        }

        if (!$vo->getSqUnidadeSuperior() instanceof UnidadeOrgValueObject) {
            $vo->setSqUnidadeSuperior(NULL);
        }
        if ($vo->getCoUnidadeGestora() == 0) {
            $vo->setCoUnidadeGestora(NULL);
        }
        if ($vo->getCoCnuc() == 0) {
            $vo->setCoCnuc(NULL);
        }

        if (is_null($vo->getStValorLiberadoGru())) {
            $vo->getStValorLiberadoGru(FALSE);
        }

        return $this;
    }

    /**
     * Altara o status da Unidade Organizacional
     *
     * @param UnidadeOrgValueObject $unidadeOrgVo
     * @param type $stAtivo
     * @return string mensagem da operação realizada
     * @throws BusinessException
     */
    public function toggleStatus ( UnidadeOrgValueObject $unidadeOrgVo, $stAtivo )
    {
        $this->transaction();
        try {

            //valida a inativação
            if (false === $stAtivo) {
                $this->_validateToInativate($unidadeOrgVo);
            }

            //altera status da unidade
            $sqPessoa = $unidadeOrgVo->getSqPessoa()->getSqPessoa();

            $this->getModel()->updateStatus($sqPessoa, $stAtivo);

            //altera status da pessoa
            $pessoaBusiness = PessoaBusiness::factory(NULL, 'libcorp');
            $pessoaVo = $pessoaBusiness->find($sqPessoa);
            $pessoaVo->setStRegistroAtivo($stAtivo);
            $pessoaBusiness->update($pessoaVo);

            $this->commit();

            if ($stAtivo) {
                return 'Unidade Interna reativada com sucesso.';
            } else {
                return 'Unidade Interna inativada com sucesso.';
            }

        } catch (ModelException $mExcp) {
            $this->rollback();
            throw new BusinessException($mExcp->getMessage(), $mExcp->getCode());
        } catch (ModelException $bExcp) {
            $this->rollback();
            throw $bExcp;
        }
    }

    private function _validateToInativate(UnidadeOrgValueObject $vo)
    {
        if ($vo->getStAtivo() === false) {
            throw new BusinessException(self::$_arrMsg[self::CODE_VALIDATE_UNIDADE_INATIVA], self::CODE_VALIDATE_UNIDADE_INATIVA);
        }

        //se existe filhas ativas não pode inativar
        $activeChildSuperior = $this->getModelPersist('libcorp')
                                    ->findActiveChildForUnidadeSuperior($vo)
                                    ->getAllDataViewObject();
        if ($activeChildSuperior->count() > 0) {
            throw new BusinessException(self::$_arrMsg[self::CODE_VALIDATE_INATIVA_SUPERIOR], self::CODE_VALIDATE_INATIVA_SUPERIOR);
        }

        $activeChildAdmPai   = $this->getModelPersist('libcorp')->findActiveChildForUnidadeAdmPai($vo)->getAllDataViewObject();
        if ($activeChildAdmPai->count() > 0) {
            throw new BusinessException(self::$_arrMsg[self::CODE_VALIDATE_INATIVA_ADM_PAI], self::CODE_VALIDATE_INATIVA_ADM_PAI);
        }

        $activeChildFinPai   = $this->getModelPersist('libcorp')->findActiveChildForUnidadeFinPai($vo)->getAllDataViewObject();
        if ($activeChildFinPai->count() > 0) {
            throw new BusinessException(self::$_arrMsg[self::CODE_VALIDATE_INATIVA_FIN_PAI], self::CODE_VALIDATE_INATIVA_FIN_PAI);
        }
    }


    /**
     * Efetua a busca de Unidades Organizacionais por UF (<b>EstadoValueObject</b>::<i>sqEstado</i>)
     *
     * @example UnidadeOrgBusiness::findByUf
     * @code
     *     # cria filtro usado pelo email
     *     $voEstado = ValueObjectAbstract::factory('fullnamespace');
     *     # outra forma de utilizar
     *     # $voEstado = EstadoValueObject::factory();
     *     $voEstado->setSqEstado(1);
     *
     *     # efetua pesquisa
     *     $uniOrgBussiness = UnidadeOrgBusiness::factory();
     *     $uniOrgBussiness->findByUf($voEstado);
     * <?php
     * @endcode
     *
     * @param EstadoValueObject $voEstado
     * @return DataViewObjectAbstract[]
     * @throws BusinessException
     */
    public function findByUf (EstadoValueObject $voEstado)
    {
        try {
            return $this->_findByUf($voEstado)->getAllDataViewObject();
        } catch (ModelException $mExcp) {
            throw new BusinessException($mExcp->getMessage());
        }
    }

    /**
     * Metodo auxiliar para pesquisa por UF
     * @param EstadoValueObject $voEstado
     */
    private function _findByUf(EstadoValueObject $voEstado)
    {
        try {
            return $this->getModelPersist('libcorp')->findByUf($voEstado);
        } catch (ModelException $mExcp) {
            throw new BusinessException($mExcp->getMessage());
        }
    }

    /**
     * Efetua a busca em Unidades Descentralizadas por Nome Pessoa (<b>PessoaValueObject</b>::<i>noPessoa</i>)
     * Solicitacao efetuada via ticket #459
     *
     * @example UnidadeOrgBusiness::findUndDescentralizadaByNome
     * @code
     * <?php
     *     # cria filtro usado pelo email
     *     $voPessoa = ValueObjectAbstract::factory('fullnamespace');
     *     # outra forma de utilizar
     *     # $voPessoa = PessoaValueObject::factory();
     *     $voPessoa->setnoPessoa('foo');
     *
     *     # efetua pesquisa
     *     $uniOrgBussiness = UnidadeOrgBusiness::factory();
     *     $uniOrgBussiness->findUndDescentralizadaByNome($voPessoa);
     * ?>
     * @endcode
     *
     * @param PessoaValueObject $voPessoa
     * @return DataViewObjectAbstract[]
     * @throws BusinessException
     */
    public function findUndDescentralizadaByNome (PessoaValueObject $voPessoa)
    {
        try {
            return $this->_findUndDescentralizadaByNome($voPessoa)->getAllDataViewObject();
        } catch (ModelException $mExcp) {
            throw new BusinessException($mExcp->getMessage());
        }
    }

    /**
     * Método auxiliar que efetua a busca em unidades Descentralizadas por Nome Pessoa
     * @param PessoaValueObject $voPessoa
     * @throws BusinessException
     */
    private function _findUndDescentralizadaByNome (PessoaValueObject $voPessoa)
    {
        try {
            return $this->getModelPersist('libcorp')->findundDescentralizadaByNome($voPessoa);
        } catch (ModelException $mExcp) {
            throw new BusinessException($mExcp->getMessage());
        }
    }

    /**
     * Efetua busca em Unids Descentralizadas por Categoria (<b>TipoUnidadeOrgValueObject</b>::<i>noTipoUnidadeOrg</i>)
     * Solicitacao efetuada via ticket #459
     *
     * @example UnidadeOrgBusiness::findUndDescentralizadaByCategoria
     * @code
     * <?php
     *     # cria filtro usado pelo email
     *     $voTipoUnidade = ValueObjectAbstract::factory('fullnamespace');
     *     # outra forma de utilizar
     *     # $voTipoUnidade = TipoUnidadeOrgValueObject::factory();
     *     $voTipoUnidade->setNoTipoUnidadeOrg('foo');
     *
     *     # efetua pesquisa
     *     $uniOrgBussiness = UnidadeOrgBusiness::factory();
     *     $uniOrgBussiness->findUndDescentralizadaByCategoria($voTipoUnidade);
     * ?>
     * @endcode
     *
     * @param TipoUnidadeOrgValueObject $voTipoUnidade
     * @throws BusinessException
     * @return DataViewObject[]
     */
    public function findUndDescentralizadaByCategoria (TipoUnidadeOrgValueObject $voTipoUnidade)
    {
        try {
            return $this->_findUndDescentralizadaByCategoria($voTipoUnidade)->getAllDataViewObject();
        } catch (ModelException $mExcp) {
            throw new BusinessException($mExcp->getMessage());
        }
    }

    /**
     * Método auxiliar que efetua a busca em unidades Descentralizadas por Categoria
     * @param TipoUnidadeOrgValueObject $voTipoUnidade
     * @throws BusinessException
     */
    private function _findUndDescentralizadaByCategoria (TipoUnidadeOrgValueObject $voTipoUnidade)
    {
        try {
            return $this->getModelPersist('libcorp')->findUndDescentralizadaByCategoria($voTipoUnidade);
        } catch (ModelException $mExcp) {
            throw new BusinessException($mExcp->getMessage());
        }
    }

    /**
     * Efetua a busca em Unidades Descentralizadas por Cidade (<b>MunicipioValueObject</b>::<i>noMunicipio</i>)
     * Solicitacao efetuada via ticket #459
     *
     * @example UnidadeOrgBusiness::findUndDescentralizadaByCidade
     * @code
     * <?php
     *     # cria filtro usado pelo email
     *     $voMunicipio = ValueObjectAbstract::factory('fullnamespace');
     *     # outra forma de utilizar
     *     # $voMunicipio = MunicipioValueObject::factory();
     *     $voMunicipio->setNoMunicipio('foo');
     *
     *     # efetua pesquisa
     *     $uniOrgBussiness = UnidadeOrgBusiness::factory();
     *     $uniOrgBussiness->findUndDescentralizadaByCidade($voMunicipio);
     * ?>
     * @endcode
     *
     * @param MunicipioValueObject $voMunicipio
     * @throws BusinessException
     * @return DataViewObject[]
     */
    public function findUndDescentralizadaByCidade (MunicipioValueObject $voMunicipio)
    {
        try {
            return $this->_findUndDescentralizadaByCidade($voMunicipio)->getAllDataViewObject();
        } catch (ModelException $mExcp) {
            throw new BusinessException($mExcp->getMessage());
        }
    }

    /**
     * Método auxiliar que efetua a busca em unidades Descentralizadas por Cidade
     * @param MunicipioValueObject $voMunicipio
     * @throws BusinessException
     */
    private function _findUndDescentralizadaByCidade (MunicipioValueObject $voMunicipio)
    {
        try {
            return $this->getModelPersist('libcorp')->findUndDescentralizadaByCidade($voMunicipio);
        } catch (ModelException $mExcp) {
            throw new BusinessException($mExcp->getMessage());
        }
    }

    /***/
    public function findByParamFilterByName (UnidadeOrgValueObject $voUnidOrg, $limit = 10, $offset = 0)
    {
        $result = $this->getModelPersist('libcorp')->findByParamFilterByName($voUnidOrg, $limit, $offset);
        // $result = $this->getModelPersist('libcorp')->findByCpf($voPessoaFisica);
        // return $result->getValueObject();
        return $result->getAllDataViewObject();
    }

    /**
     * @param PessoaValueObject $filterPessoa
     * @param integer $limit
     * @return br\gov\sial\core\valueObject\DataViewObject[]
     * @throws BusinessException
     */
    public function findUnidadeConservacao (PessoaValueObject $filterPessoa, $limit = NULL)
    {
        try {
            return $this->getModelPersist('libcorp')
                        ->findUnidadeConservacao($filterPessoa, $limit)
                        ->getAllDataViewObject();
        } catch (ModelException $exp) {
            throw new BusinessException(Messages::ERROR, $exp->getCode(), $exp);
        }
    }

    public function findUnidadeOrgAtivaById (UnidadeOrgValueObject $voUnidOrg)
    {
        try {
            $voUnidOrg->setStAtivo(TRUE);
            return $this->getModelPersist('libcorp')
                         ->findByParam($voUnidOrg)
                         ->getAllDataViewObject();
        } catch (ModelException $exp) {
            throw new BusinessException(Messages::ERROR, $exp->getCode(), $exp);
        }
    }


    /**
     * validação de cadastro
     *
     * @param UnidadeOrgValueObject $unidadeOrgVo
     * @param PessoaValueObject $pessoaVo
     * @throws BusinessException
     */
    private function _validateToSave (UnidadeOrgValueObject $unidadeOrgVo, PessoaValueObject $pessoaVo) {

        $result = $this->getModel()->getUnidadeOrgByName($pessoaVo);

        if ($result->getAllValueObject()->count()) {
            throw new BusinessException(
                    self::$_arrMsg[self::CODE_VALIDATE_NO_PESSOA_DUPLICATED],
                    self::CODE_VALIDATE_NO_PESSOA_DUPLICATED);
        }

        $resultSigla = $this->getModel()->getUnidadeOrgBySigla($unidadeOrgVo);

        if ($resultSigla->getAllValueObject()->count()) {
            throw new BusinessException(
                    self::$_arrMsg[self::CODE_VALIDATE_SG_UNIDADE_ORG_DUPLICATED],
                    self::CODE_VALIDATE_SG_UNIDADE_ORG_DUPLICATED);
        }

        IllegalArgumentException::throwsExceptionIfParamIsNull(
                trim($pessoaVo->getNoPessoa()),
                self::$_arrMsg[self::CODE_VALIDATE_NO_PESSOA],
                self::CODE_VALIDATE_NO_PESSOA);
        IllegalArgumentException::throwsExceptionIfParamIsNull(
                trim($unidadeOrgVo->getSgUnidadeOrg()),
                self::$_arrMsg[self::CODE_VALIDATE_SG_UNIDADE_ORG],
                self::CODE_VALIDATE_SG_UNIDADE_ORG);
        IllegalArgumentException::throwsExceptionIfParamIsNull(
                trim($unidadeOrgVo->getSqTipoUnidade()),
                self::$_arrMsg[self::CODE_VALIDATE_SQ_TIPO_UNIDADE],
                self::CODE_VALIDATE_SQ_TIPO_UNIDADE);

        return $this;
    }

    /**
     * validação de alteração
     *
     * @param UnidadeOrgValueObject $unidadeOrgVo
     * @param PessoaValueObject $pessoaVo
     * @throws BusinessException
     */
    private function _validateToUpdate (UnidadeOrgValueObject $unidadeOrgVo, PessoaValueObject $pessoaVo)
    {
        $sqPessoa = $pessoaVo->getSqPessoa();

        BusinessException::throwsExceptionIfParamIsNull(
                $sqPessoa,
                self::$_arrMsg[self::CODE_REQUIRED_SQ_PESSOA],
                self::CODE_REQUIRED_SQ_PESSOA);

        $pessoaVoTemp = PessoaBusiness::factory()->find($sqPessoa);

        if($pessoaVo->getNoPessoa() != $pessoaVoTemp->getNoPessoa() ) {
            $pessoaVo->setSqPessoa(NULL);
            $resultNome =  $this->getModel()->getUnidadeOrgByName($pessoaVo);

           if ($resultNome->getAllValueObject()->count()) {
                throw new BusinessException(
                        self::$_arrMsg[self::CODE_VALIDATE_NO_PESSOA_DUPLICATED],
                        self::CODE_VALIDATE_NO_PESSOA_DUPLICATED);
            }

            $pessoaVo->setSqPessoa($pessoaVoTemp->getSqPessoa());
        }

        $unidadeOrgVoTemp = $this->find($pessoaVo->getSqPessoa());

        if($unidadeOrgVo->getSgUnidadeOrg() != $unidadeOrgVoTemp->getSgUnidadeOrg() ){
            $unidadeOrgVo->setSqPessoa(NULL);
            $resultSigla = $this->getModel()->getUnidadeOrgBySigla($unidadeOrgVo);

            if ($resultSigla->getAllValueObject()->count()) {
                throw new BusinessException(
                        self::$_arrMsg[self::CODE_VALIDATE_SG_UNIDADE_ORG_DUPLICATED],
                        self::CODE_VALIDATE_SG_UNIDADE_ORG_DUPLICATED);
            }

            $unidadeOrgVo->setSqPessoa($pessoaVo->getSqPessoa());
        }

        IllegalArgumentException::throwsExceptionIfParamIsNull(
                trim($pessoaVo->getNoPessoa()),
                self::$_arrMsg[self::CODE_VALIDATE_NO_PESSOA],
                self::CODE_VALIDATE_NO_PESSOA);
        IllegalArgumentException::throwsExceptionIfParamIsNull(
                trim($unidadeOrgVo->getSgUnidadeOrg()),
                self::$_arrMsg[self::CODE_VALIDATE_SG_UNIDADE_ORG],
                self::CODE_VALIDATE_SG_UNIDADE_ORG);
        IllegalArgumentException::throwsExceptionIfParamIsNull(
                trim($unidadeOrgVo->getSqTipoUnidade()),
                self::$_arrMsg[self::CODE_VALIDATE_SQ_TIPO_UNIDADE],
                self::CODE_VALIDATE_SQ_TIPO_UNIDADE);

        return $this;
    }

    /**
     * Busca as Unidades Organizacionais por Tipo de Unidade Organizacional
     *
     * @param TipoUnidadeOrgValueObject $voTipoUnidadeOrg
     * @throws BusinessException
     */
    public function findUnidadeOrgByTipoUorg(TipoUnidadeOrgValueObject $voTipoUnidadeOrg)
    {
        return $this->getModel()->findUnidadeOrgByTipoUorg($voTipoUnidadeOrg)->getAllValueObject();;
    }
}